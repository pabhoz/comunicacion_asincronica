<?php

	/**
	* 
	*/
	class Session
	{
		
		public static function init(){
			@session_start();
		}

		public static function destroy(){
			unset($_SESSION);
			session_destroy();
		}

		public static function set($key,$value){
			$key = strtoupper ( $key);
			$_SESSION[$key] = $value;
		}

		public static function get($key){
			$key = strtoupper ( $key);
			return (isset($_SESSION[$key]))? $_SESSION[$key] : false;
		}


	}

	?>